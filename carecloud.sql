-- MySQL dump 10.13  Distrib 5.6.14, for Linux (x86_64)
--
-- Host: localhost    Database: carecloud
-- ------------------------------------------------------
-- Server version	5.6.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(10) NOT NULL,
  `middle_name` varchar(10) NOT NULL,
  `last_name` varchar(10) NOT NULL,
  `gender` bit(1) NOT NULL,
  `birthdate` date NOT NULL,
  `phone1` varchar(12) NOT NULL,
  `phone1_type` bit(1) NOT NULL,
  `phone2` varchar(12) DEFAULT NULL,
  `phone2_type` bit(1) DEFAULT NULL,
  `phone3` varchar(12) DEFAULT NULL,
  `phone3_type` bit(1) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (1,'Ahmad','','AlFakharan','','0000-00-00','201211126112','','201211126112','','201211126112','','abohmeed@gmail.com','saas',1,'2014-01-30 18:43:20','2014-01-30 18:43:20'),(2,'Ahmad','','AlFakharan','','0000-00-00','201211126112','','201211126112','','201211126112','','abohmeed@gmail.com','saas',1,'2014-01-30 18:48:50','2014-01-30 18:48:50'),(3,'Ahmad','','AlFakharan','','1979-05-11','201211126112','','201211126112','','201211126112','','abohmeed@gmail.com','saas',1,'2014-01-30 18:54:19','2014-01-30 18:54:19'),(4,'Ahmad','','AlFakharan','','1970-11-06','201211126112','','201211126112','','201211126112','','abohmeed@gmail.com','',1,'2014-01-30 19:07:34','2014-01-30 19:07:34'),(5,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 19:31:46','2014-01-30 19:31:46'),(6,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 19:34:29','2014-01-30 19:34:29'),(7,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 19:59:38','2014-01-30 19:59:38'),(8,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 20:02:06','2014-01-30 20:02:06'),(9,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 20:18:09','2014-01-30 20:18:09'),(10,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 20:20:39','2014-01-30 20:20:39'),(11,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 20:22:40','2014-01-30 20:22:40'),(12,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 20:23:05','2014-01-30 20:23:05'),(13,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 20:28:21','2014-01-30 20:28:21'),(14,'','','','','0000-00-00','','','','','','','','',1,'2014-01-30 20:43:54','2014-01-30 20:43:54');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phone_types`
--

DROP TABLE IF EXISTS `phone_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phone_types` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `value` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phone_types`
--

LOCK TABLES `phone_types` WRITE;
/*!40000 ALTER TABLE `phone_types` DISABLE KEYS */;
INSERT INTO `phone_types` VALUES (1,'mobile','محمول'),(2,'landline','أرضي');
/*!40000 ALTER TABLE `phone_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-30 21:00:32
