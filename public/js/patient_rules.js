$(document).ready(function(){
	//Overriding plugin defaults for compatibility with bootstrap framework
	$.validator.setDefaults({
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    }
	});
	//Get the current year to be used in date validation
	currentYear = new Date().getFullYear()
	minYear = currentYear - 100;
	$('#patientForm').validate({
		rules: {
			firstname: "required",
			middlename: "required",
			lastname: "required",
			gender: "required",
			day: {
				required: true,
				range: [1,31]
			},
			month: {
				required: true,
				range: [1,12]
			},
			year: {
				required: true,
				range: [minYear,currentYear]
			},
			phone1: {
				required: true,
				number: true,
				remote: {
					url: "/patients/check_phone",
					type: "post",
					data: {
						phone1: function(){
							return $('#phone1').val();
						}
					}
				}
			},
			phone1type: {
				required: true,
				range: [1,2]
			},
			phone2type: {
				range: [1,2]
			},
			phone3type: {
				range: [1,2]
			},
			email: {
				email: true
			}
		},
		messages: {
			phone1: {
				remote: 'هذا الرقم مستخدم بالفعل'
			}
		}
	});
});