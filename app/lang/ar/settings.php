<?php
return array(
	'general' => 'عام',
	'users' => 'المستخدمون والأطباء',
	'billable_items' => 'الخدمات والمنتجات',
	'appointment_types' => 'أنواع الكشف',
	'payment_types' => 'طرق الدفع',
	'taxes' => 'الضرائب',
	);