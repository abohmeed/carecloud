<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
    public function __construct(){
        //The following variables are global variables that need to be present in each and every page of the application
        $this->bag = array(
            'title'=>'Please add a title to the page',
            'active_tab' => '',
            'has_sidebar' => false,
            'js' => array(),
            'css' => array(),
        );
    } 
     
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}