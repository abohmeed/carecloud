<?php

class PatientController extends \BaseController {

	public function __construct(){
		parent::__construct();
		$this->bag['active_tab'] = 'patients';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->bag['title'] = trans('titles.patients');
		//Add the jquery file for various functions required in the index file
		$this->bag['js'] = array('patients.js');
		return View::make('patients.index')->with('bag',$this->bag);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//Add the JQuery validation plugin to the page. The patient_rules.js file contains the validaton logic that I wrote.
		$this->bag['js'] = array('jquery.validate.min.js','messages_ar.js','patient_rules.js');
		$this->bag['title'] = trans('titles.add_patient');
		return View::make('patients.create')->with('bag',$this->bag);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		/*
			** NOTICE ** The patient ID is a composite ID consisting of the id field and the clinic id. This is to prevent privacy breaches.
		*/

		//Validating the input before storing on the database
		$rules = array(
			'firstname' => 'required',
			'middlename' => 'required',
			'lastname' => 'required',
			'gender' => 'required',
			'day' => 'required|numeric|between:1,31',
			'month' => 'required|numeric|between:1,12',
			'year' => 'required|numeric',
			'phone1' => 'required|numeric',
			'phone2' => 'numeric',
			'phone2type' => 'numeric|between:1,2',
			'phone3' => 'numeric',
			'phone3type' => 'numeric|between:1,2',
			'email' => 'email',
			// 'clinic_id' => 'required|numeric'
			);
		$validator = Validator::make(Input::all(),$rules);
		if ($validator->fails()){
			return Redirect::to('/patients/create')
				->withErrors($validator)
				->withInput();
		} else {

			//Adjust birhtdate to conform to MySQL date format
			$day = Input::get('day');
			$month = Input::get('month');
			$year = Input::get('year');
			$birthdate = $year.'-'.$month.'-'.$day;


			$patient = Patient::create(array(
				'clinic_id' 	=> 2, //To be changed to be the currently logged in clinic id
				'first_name' 	=> e(Input::get('firstname')),
				'middle_name' 	=> e(Input::get('middlename')),
				'last_name' 	=> e(Input::get('lastname')),
				'gender' 		=> e(Input::get('gender')),
				'birthdate' 	=> $birthdate,
				'phone1' 		=> Input::get('phone1'),
				'phone1_type' 	=> Input::get('phone1type'),
				'phone2' 		=> Input::get('phone2'),
				'phone2_type' 	=> Input::get('phone2type'),
				'phone3' 		=> Input::get('phone3'),
				'phone3_type' 	=> Input::get('phone3type'),
				'email'			=> Input::get('email'),
				'address' 		=> e(Input::Get('address')),
				));
			//Get the new patient id to be displayed to the user, padded with zeroes. Note that the patient ID is a composite of the patient id and the clinic id
			// $patient_id = Patient::get_patient_id($patient->id);
			// $patient_id = $patient->get_unique_id();
			// Session::flash('message', trans('patients.add_success',array('id' => $patient_id)));
			Session::flash('message', trans('patients.add_success',array('id' => $patient->id)));
			return Redirect::to('patients');
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	//The filter function 

	public function patient_filter(){
		//We can filter by id, name, or phone number. This is restricted to the currently logged in clinic
		$needle = e(Input::get('patients-filter'));
		$patients = Patient::where(function($query) {
						$query->where('clinic_id','=',1);
						})
						->where(function($query) use ($needle) {
									$query->where('id','=',$needle)
									->orWhere('first_name', 'LIKE' , "$needle%")
									->orWhere('middle_name', 'LIKE' , "%$needle%")
									->orWhere('last_name', 'LIKE' , "%$needle%")
									->orWhere('phone1', 'LIKE' , "%$needle%");
								})->get();
		return View::make('patients.ajax.list')->with('patients',$patients);
	}

	//Phone validattion function

	public function check_phone(){
		$phone = e(Input::get('phone1'));
		if (Patient::whereNested(function($query) use ($phone){
								$query->where('clinic_id','=',1); //Change this
								$query->where('phone1','=',$phone);
							})->get()->count() > 0)
			return 'false';
		else
			return 'true';
	}

}