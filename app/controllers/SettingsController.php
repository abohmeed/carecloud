<?php
class SettingsController extends BaseController {

	public function __construct(){
		parent::__construct();
	}

	public function getIndex(){
		$this->bag['title'] = Lang::get('titles.settings');
		$this->bag['active_tab'] = 'settings';
		$this->bag['active_link'] = 'settings';
		$this->bag['has_sidebar'] = true;
		return View::make('settings.index')->with('bag',$this->bag);
	}
}