<!DOCTYPE html>

<html lang="ar">
<head>
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/bootstrap-rtl.min.css" type="text/css">
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css" type="text/css">
    <link rel="stylesheet" href="/css/styles.css" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$bag['title']}}</title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1>مرحبا بكم في نظام تقارير المرضى</h1>
      </div>
      <div class="col-sm-12" style="margin-bottom:10px;">
        <nav class="nav navbar-default" role='navigation'>
          <div class="navbar-header">
            <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#navbar-collapse-1'>
              <span class="sr-only">{{Lang::get('nav.toggle')}}</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href='/'>{{Lang::get('nav.brand')}}</a>
          </div>
          <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li {{$bag['active_tab'] == 'home' ? 'class="active"' : ''}}><a href="/">{{Lang::get('nav.home')}}</a> </li>
              <li {{$bag['active_tab'] == 'calendar' ? 'class="active"' : ''}}><a href="/calendar">{{Lang::get('nav.calendar')}}</a> </li>
              <li {{$bag['active_tab'] == 'patients' ? 'class="active"' : ''}}><a href="/patients">{{Lang::get('nav.patients')}}</a> </li>
              <li {{$bag['active_tab'] == 'invoices' ? 'class="active"' : ''}}><a href="/invoices">{{Lang::get('nav.invoices')}}</a> </li>
              <li {{$bag['active_tab'] == 'payments' ? 'class="active"' : ''}}><a href="/payments">{{Lang::get('nav.payments')}}</a> </li>
              <li {{$bag['active_tab'] == 'products' ? 'class="active"' : ''}}><a href="/products">{{Lang::get('nav.products')}}</a> </li>
              <li {{$bag['active_tab'] == 'expenses' ? 'class="active"' : ''}}><a href="/expenses">{{Lang::get('nav.expenses')}}</a> </li>
              <li {{$bag['active_tab'] == 'contacts' ? 'class="active"' : ''}}><a href="/contacts">{{Lang::get('nav.contacts')}}</a> </li>
              <li {{$bag['active_tab'] == 'reports' ? 'class="active"' : ''}}><a href="/reports">{{Lang::get('nav.reports')}}</a> </li>
              <li {{$bag['active_tab'] == 'settings' ? 'class="active"' : ''}}><a href="/settings">{{Lang::get('nav.settings')}}</a> </li>
            </ul>
          </div>
        </nav>
      </div><!-- end navigation div -->
      @if ($bag['has_sidebar'])
      <div class="col-sm-2">
        @yield('sidebar')
      </div>
      @endif
      <div class="col-md-12">
        @yield('content')
      </div>
      <div id="footer" class="col-md-12">
          <p>This is the footer</p>
      </div>
    </div> <!-- End of tow -->
  </div> <!-- End of container -->

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="/js/jquery-2.1.0.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/bootstrap.min.js"></script>
  <!-- Add extra js files specified by the controller -->
  <?php foreach ($bag['js'] as $file){
    echo ("<script src='/js/$file'></script>");
  }
  ?>
</body>
</html>
