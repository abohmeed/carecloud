@extends('master')
@section('content')
<h2>{{trans('patients.add_patient')}}</h2>
{{ HTML::ul($errors->all(),array('class'=>'error')) }}
<form role='form' class="form-horizontal col-md-12" id='patientForm' method="POST" action="/patients" novalidate>
	<h2 class="header">{{trans('patients.personal_details')}}</h2>
	<div class="form-group col-md-4">
		<label for="firstname">{{trans('patients.firstname')}}</label>
		<input class="form-control" type="text" name="firstname" id="firstname" value="{{Input::old('firstname')}}" required>
	</div>
	<div class="form-group col-md-4">
		<label for="middlename">{{trans('patients.middlename')}}</label>
		<input class="form-control" type="text" name="middlename" id="middlename" required>
	</div>
	<div class="form-group col-md-4">
		<label for="lastname">{{trans('patients.lastname')}}</label>
		<input class="form-control" type="text" name="lastname" id="lastname">
	</div>
	<div class="form-group col-md-2">
		<label>{{trans('patients.birthdate')}}</label>
	</div>
	<div class="form-group col-md-1">
		<label for="day">{{trans('patients.day')}}</label>
		<input class="form-control" type="text" name="day" id="day">
	</div>
	<div class="form-group col-md-1">
		<label for="month">{{trans('patients.month')}}</label>
		<input class="form-control" type="text" name="month" id="month">
	</div>
	<div class="form-group col-md-2">
		<label for="year">{{trans('patients.year')}}</label>
		<input class="form-control" type="text" name="year" id="year">
	</div>
	<div class="form-group col-md-1">
		<label class="control-label">{{trans('patients.type')}}</label>
	</div>	
	<div class="radio col-md-1">
		<label class="radio-inline">
			<input type="radio" name="gender" id="gender" value="0" checked> {{trans('patients.male')}}
		</label>
	</div>
	<div class="radio col-md-1">
		<label class="radio-inline">
			<input type="radio" name="gender" id="gender" value="1"> {{trans('patients.female')}}
		</label>
	</div>
	<h3 class="header col-md-12">{{trans('patients.contact_information')}}</h3>
	<div class="form-group col-md-2">
		<label for="phone1">{{trans('patients.phone')}} 1</label>
		<input class="form-control" type="text" name="phone1" id="phone1">
	</div>
	<div class="form-group col-md-2">
		<label>{{trans('patients.type')}}</label>
		<select class="form-control" name="phone1type">
			<option value="1">{{trans('patients.mobile')}}</option>
			<option value="2">{{trans('patients.landline')}}</option>
		</select>
	</div>
	<div class="form-group col-md-2">
		<label for="phone2">{{trans('patients.phone')}} 2</label>
		<input class="form-control" type="text" name="phone2" id="phone2">
	</div>
	<div class="form-group col-md-2">
		<label>{{trans('patients.type')}}</label>
		<select class="form-control" name="phone2type">
			<option value="1">{{trans('patients.mobile')}}</option>
			<option value="2">{{trans('patients.landline')}}</option>
		</select>
	</div>
	<div class="form-group col-md-2">
		<label for="phone3">{{trans('patients.phone')}} 3</label>
		<input class="form-control" type="text" name="phone3" id="phone1">
	</div>
	<div class="form-group col-md-2">
		<label>{{trans('patients.type')}}</label>
		<select class="form-control" name="phone3type">
			<option value="1">{{trans('patients.mobile')}}</option>
			<option value="2">{{trans('patients.landline')}}</option>
		</select>
	</div>
	<div class="form-group col-md-4">
		<label for="email">{{trans('patients.email')}}</label>
		<input class="form-control" type="email" name="email" id="email">
	</div>
	<h3 class="header col-md-12">{{trans('patients.address')}}</h3>
	<div class="form-group col-md-12">
		<label for="address">{{trans('patients.address')}}</label>
		<textarea class="form-control" rows="3" name="address" id="address"></textarea>
	</div>
	<div class="col-md-3">
		<button type="submit" class="btn btn-success btn-block">{{trans('patients.save_patient')}}</button>
	</div>
</form>
@endsection