@extends('master')
@section('content')
<!-- Show any status messages -->
@if (Session::has('message'))
	<div class="row">
		<div class="alert alert-success alert-dismissable col-md-5" data-alert="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{ Session::get('message') }}
		</div>
	</div>
@endif
<h2>{{Lang::get('titles.patients')}}</h2>
	<div class="form-group col-sm-5">
		<input type="text" class="form-control" id="patients-filter" name="patients-filter" placeholder="{{Lang::get('patients.search_place_holder')}}">
	</div>
<div class="col-md-3">
	<button class="btn btn-primary" onclick="javascript:window.location.href='/patients/create'">{{Lang::get('patients.add_patient')}}</button>
</div>
<!-- This is the placeholder for the patients list. It will be populated with when the AJAX call is made by searching in the text field -->
<div class="col-md-12" id="patients-list"></div>
@endsection