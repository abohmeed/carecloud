<!doctype html>
<html>
	<head>
		<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
	    <link rel="stylesheet" href="/css/bootstrap-rtl.min.css" type="text/css">
	    <link rel="stylesheet" href="/css/bootstrap-theme.min.css" type="text/css">
	    <link rel="stylesheet" href="/css/styles.css" type="text/css">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	</head>
	<body>
		<?php echo PatientController::formatPatientId(5); ?>
		 <script src="/js/jquery-2.1.0.min.js"></script>
  		<!-- Include all compiled plugins (below), or include individual files as needed -->
  		<script src="/js/bootstrap.min.js"></script>
	</body>
</html>