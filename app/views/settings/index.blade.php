@extends('master')
@section('sidebar')
<div id="sidebar">
	<h2>{{Lang::get('titles.settings')}}</h2>
	<ul>
		<li><a {{$bag['active_link'] == 'settings' ? 'class="selected"' : ''}} href="/settings">{{Lang::get('settings.general')}}</a></li>
		<li><a {{$bag['active_link'] == 'users' ? 'class="selected"' : ''}} href="/settings/users">{{Lang::get('settings.users')}}</a></li>
		<li><a {{$bag['active_link'] == 'billable' ? 'class="selected"' : ''}} href="/settings/billable">{{Lang::get('settings.billable_items')}}</a></li>
		<li><a {{$bag['active_link'] == 'appointment_types' ? 'class="selected"' : ''}} href="/settings/appointment_types">{{Lang::get('settings.appointment_types')}}</a></li>
		<li><a {{$bag['active_link'] == 'payment_types' ? 'class="selected"' : ''}} href="/settings/payment_types">{{Lang::get('settings.payment_types')}}</a></li>
		<li><a {{$bag['active_link'] == 'taxes' ? 'class="selected"' : ''}} href="/settings/taxes">{{Lang::get('settings.taxes')}}</a></li>
	</ul>
</div>
@endsection
@section('content')
@endsection