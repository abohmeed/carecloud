<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('test',function(){
	return View::make('test');
});
Route::post('/patients/check_phone', 'PatientController@check_phone');
Route::post('/patients/patient_filter', 'PatientController@patient_filter');
Route::resource('patients','PatientController');
Route::controller('settings','SettingsController');
Route::controller('/','HomeController');